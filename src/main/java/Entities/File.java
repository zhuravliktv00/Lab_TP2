package Entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class File {
    private String format;

    private byte[] bytes;

    public File() {
    }
    public String getFormat() {
        return format;
    }
    public void setFormat(String format) {
        this.format = format;
    }
    public byte[] getBytes() {
        return bytes;
    }
    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
