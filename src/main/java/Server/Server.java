package Server;

import java.io.*;
import java.net.Socket;

public class Server extends Thread{
    private Socket socket;

    private BufferedReader in;

    private BufferedWriter out;

    public Server(Socket socket) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        start();
    }

    public void run() {
        String json;
        try {
            while (true) {
                json = in.readLine();
                if (json.equals("exit")) {
                    break;
                }
                for (Server vr : Connection.serverList) {
                    if (vr.socket.equals(this.socket)) {
                        continue;
                    }
                    vr.send(json);
                }
            }

        } catch (IOException e) {
        }
    }

    private void send(String json) {
        try {
            out.write(json + "\n");
            out.flush();
        } catch (IOException ignored) {
        }
    }
}
